
#EXERCIES 4.4.1

class User
  attr_accessor :first_name,:last_name, :email
  def initialize(attributes = {})
    @first_name = attributes[:first_name]
    @last_name = attributes[:last_name]
    @email = attributes[:email]
  end
  def formatted_email
    "#{@first_name} #{@last_name} <#{@email}>"
  end

  def full_name
    "#{@first_name} #{@last_name}"
  end

  #EXERCIES 4.4.2
  def alphabetical_name
    "#{@first_name},#{@last_name}"
  end
end

p user = {:first_name => "toan", :last_name => "do", :email => "@vti.com"}
p example = User.new(user)
p x =example.full_name
#EXERCIES 4.4.2
p y =example.alphabetical_name
#EXERCIES 4.4.3
p y.split(',')
p y.split(',') == x.split(' ')
